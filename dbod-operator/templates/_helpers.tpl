{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "dbod-operator.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "dbod-operator.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create the namespace name for all the resources of the operator
*/}}
{{- define "dbod-operator.namespace" -}}
{{- .Values.operatorNamespace.name | default (include "dbod-operator.name" .) | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "dbod-operator.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "dbod-operator.labels" -}}
app.kubernetes.io/name: {{ include "dbod-operator.name" . }}
helm.sh/chart: {{ include "dbod-operator.chart" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
operator: {{ .Chart.Name }}
{{- end -}}

{{/*
Create the name of the service account to use
*/}}
{{- define "dbod-operator.serviceAccountName" -}}
{{- if .Values.serviceAccount.create -}}
    {{ .Values.serviceAccount.name | default (include "dbod-operator.name" .) }}
{{- else -}}
    {{ .Values.serviceAccount.name | default "default" }}
{{- end -}}
{{- end -}}

{{/*
ImagePullSecret name
*/}}
{{- define "dbod-operator.imagePullSecretName" -}}
{{ (printf "%s-%s" (include "dbod-operator.name" .) "gitlab-deploy-token") | trunc 63 | trimSuffix "-" }}
{{- end -}}

{{- define "dbod-operator.imagePullSecret" }}
{{- printf "{\"auths\": {\"%s\": {\"auth\": \"%s\"}}}" .Values.imagePullSecret.registry (printf "%s:%s" .Values.imagePullSecret.username .Values.imagePullSecret.password | b64enc) | b64enc }}
{{- end -}}

{{/*
RBAC names
*/}}
{{- define "dbod-operator.clusterRoleName" -}}
{{ (printf "%s-%s" (include "dbod-operator.name" .) "cluster-permissions") | trunc 63 | trimSuffix "-" }}
{{- end -}}
{{- define "dbod-operator.roleName" -}}
{{ (printf "%s-%s" (include "dbod-operator.name" .) "admin") | trunc 63 | trimSuffix "-" }}
{{- end -}}
